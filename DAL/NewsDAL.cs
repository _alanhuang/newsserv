﻿using Dapper;
using MODEL;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class NewsDAL
    {
        string connection = ConfigurationManager.ConnectionStrings["connection"].ConnectionString;

        public ApiResult<NewsInfo> GetNews()
        {
            ApiResult<NewsInfo> result = new ApiResult<NewsInfo>(new List<NewsInfo> { });
            try
            {
                using (SqlConnection conn = new SqlConnection(connection))
                {
                    string sql = @"select id, type, title, creatorFrom, image from newsinfo";
                    result.Data = conn.Query<NewsInfo>(sql).ToList();
                }
            }
            catch (Exception ex)
            {
                result.Code = "500";
                result.Success = false;
                result.Message = ex.Message;
            }
            return result;
        }
        public ApiResult<NewsInfo> GetNewsDetail(NewsInfo datalist)
        {
            ApiResult<NewsInfo> result = new ApiResult<NewsInfo>(new List<NewsInfo> { });
            try
            {
                using (SqlConnection conn = new SqlConnection(connection))
                {
                    string sql = @"select * from newsinfo Where id = @id";
                    result.Data = conn.Query<NewsInfo>(sql, new { id = datalist.id }).ToList();
                }
            }
            catch (Exception ex)
            {
                result.Code = "500";
                result.Success = false;
                result.Message = ex.Message;
            }
            return result;
        }
    }
}
