﻿using DAL;
using MODEL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace NewsServ.Controllers
{
    public class NewsController : ApiController
    {
        NewsDAL newsDAL = new NewsDAL();

        public IHttpActionResult GetNews()
        {
            var result = newsDAL.GetNews();
            return Ok(result);
        }

        public IHttpActionResult GetNewsDetail([FromUri]NewsInfo datalist)
        {
            var result = newsDAL.GetNewsDetail(datalist);
            return Ok(result);
        }
    }
}
