﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MODEL
{
    public class NewsInfo
    {
        public Int32 id { set; get; }
        public string type { set; get; }
        public string title { set; get; }
        public string createDate { set; get; }
        public string updateDate { set; get; }
        public string creatorFrom { set; get; }
        public string creator { set; get; }
        public string image { set; get; }
        public string content { set; get; }
    }
}
