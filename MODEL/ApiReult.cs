﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MODEL
{
    public class ApiResult<T>
    {
        public bool Success { get; set; }
        public string Code { get; set; }
        public string Message { get; set; }
        public DateTime DataTime { get; set; }
        public List<T> Data { get; set; }


        public ApiResult() { }

        public ApiResult(List<T> data)
        {
            Code = "0";
            Success = true;
            DataTime = DateTime.Now;
            Data = data;
        }
    }

    public class ApiError : ApiResult<object>
    {
        public ApiError(string code, string message)
        {
            Code = code;
            Success = false;
            this.DataTime = DateTime.Now;
            Message = message;
        }
    }
}
